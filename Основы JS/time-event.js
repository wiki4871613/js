// Напишите функцию countEverySecond(n), которая каждую секунду печатает в консоль цифру, начиная с 1 и заканчивая {n}

let START_VAL = 1 //начало интервала
let END_VAL = 2   //конец интервала 

countEverySecond(START_VAL, END_VAL)
function showMiddleValues(startVal, endVal) {
    let midVal = startVal //Значение входящее в интервал
    const intervalId = setInterval( () => {
        if (midVal >= startVal && midVal <= endVal) {
            console.log(midVal);
        }
        midVal++
    }, 1000); //задержка перед выводом 1 сек
    setTimeout(() => clearInterval(intervalId), 16000) // доступное время 16 сек (к заданию не относится)
    return
}



// Напишите функцию pick, которая принимает на вход массив с объектами и название поля и возвращает новый массив со значениями этого поля, если оно присутствует в объекте. Если соответствующего поля нет, следует вернуть undefined.
const arr = [
  { a: 1, b: 2 },
  { a: 'value' },
  { o: null },
  { a: true, m: 123 },
]
console.log(pick(arr, 'a'));  // [1, 'value', undefined, true]
function pick(arr = [], key) {
  return arr.map(elem => elem[key])
}

//   Напишите функцию createCounter, которая принимает число и возвращает функцию-счётчик, начинающую отсчет с этого числа.
function createCounter(val = 0) {
  val = val - 1
  return function counter() {
      val += 1
      return val
  }
}

c3 = createCounter(3)
console.log(c3());
console.log(c3());
const arr2 = [1, 2, 3]

function sum(x, y) {
  return x + y
}

const fiveMultiplier = createMultiplier(sum, 5)
console.log(fiveMultiplier(2, 3));  // sum(2, 3) * 5 = (2 + 3) * 5 = 25

function createMultiplier(sum, val) {
  return function test(x, y) {
      return sum(x, y) * 5
  }
}
const superNewObj = { c: 100500 }
console.log(Object.isPrototypeOf(superNewObj) === Object.prototype);
