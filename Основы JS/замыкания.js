// Перепишите функцию так, чтобы она писала 'Hello, {name}' в консоль только при первых трёх вызовах, начиная с 4-го ничего не делала. Используйте замыкания.
let counter = 0;
const sHello = name => {
    if (counter == 3) {
        return
    }
    console.log(`Hello, ${name}`);
    counter++;
}

// Переменная, объявленная с помощью var, всплывает и видна на уровне функции, даже если объявлена внутри цикла for. Перепишите программу так, чтобы она печатала от 1 до 10 в консоль. Используйте замыкания.

let START_VAL = 1 //начало интервала
let END_VAL = 2   //конец интервала 

showMiddleValues(START_VAL, END_VAL)
function showMiddleValues(startVal, endVal) {
    let midVal = startVal //Значение входящее в интервал
    const intervalId = setInterval( () => {
        if (midVal >= startVal && midVal <= endVal) {
            console.log(midVal);
        }
        midVal++
    }, 1000); //задержка перед выводом 1 сек
    setTimeout(() => clearInterval(intervalId), 16000) // доступное время 16 сек (к заданию не относится)
    return
}
