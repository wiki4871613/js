// Сверстайте 5 параграфов с текстом. С помощью JS меняйте цвет фона каждого параграфа на случайный каждую секунду. Создайте массив с названиями цветов ['blue', 'cyan', ...] и с помощью функции из предыдущего задания выбирайте случайный цвет из массива.

const random = (min, max) => {
    const rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
  };
  const colors = ['red', 'green', 'blue'];
  const getRandomColor = () => {
      return colors[random(0, colors.length - 1)]
  };
  const paragraphs = document.querySelectorAll('p');
  setInterval(() => {
    paragraphs.forEach(p => {
        p.style.backgroundColor = getRandomColor();
    })
  }, 1000);

//   Напишите функцию, которая принимает HTML в виде строки и возвращает HTML без элементов div (и всего, что внутри), все остальные элементы сохраняются. Используйте createElement, querySelectorAll и innerHTML.
const html = '<div></div>Hello<div><div><p>Hello world</p></div></div> <b>World!</b>';

const removeDivs = html => {
    const root = document.createElement('span');
    root.innerHTML = html;
    const divs = root.querySelectorAll('div');
    divs.forEach(div => {
        div.parentNode.removeChild(div);
    })
    return root.innerHTML;
}
alert( removeDivs(html));
alert( removeDivs(html) === 'Hello <b>World!</b>'); // Должно быть true