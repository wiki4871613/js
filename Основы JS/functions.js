// Напишите функцию countDown(n), которая получает число {n} в качестве аргумента и печатает в консоль обратный отсчет, начиная с этого числа. Например, countDown(3) напечатает: 3 2 1.
function countDown(val){
    for (let i = val; i > 0; i--) {
        console.log(i);
    }
}
//Напишите функцию getPercents(percent, number), которая возвращает {percent} процентов от {number}.
function getPercents(percent, number) {
    console.log((number / 100) * percent);
}
// Напишите функцию repeatWord(word, count), которая принимает слово и количество раз для его повторения. Например, если вызвать repeatWord('слово', 3) функция напечатает "слово1, слово2, слово3" на одной строке.
function repeatWord(word, count) {
    let res = "";
    for (let i = 1; i <= count; i++) {
        res += word + " "
    }
    console.log(res);
}
// Напишите функцию createAdder(a), которая возвращает функцию addA, которая принимает b и возвращает a + b.
function createAdder(a) {
    return function (b) { return a + b };
}

// Напишите функцию sayHello(name, surname, age, greeting), которая принимает в качестве аргументов имя, фамилию, возраст и приветствие. Каждый аргумент должен иметь дефолтное значение, функция должна быть стрелочной и не иметь ключевого слова return.
const sayHello = (name = "Ivan", surname = "Ivanov", age = "10", greeting = "Hello") => {
    console.log(greeting + " ," + name + " " + surname + " age: " + age);
};
// Напишите функцию match, которая принимает 2 строки и возвращает true, если эти строки равны без учета регистра. Например, match('hEllO', 'HELLo') должен вернуть true.
function match(val1 = "", val2 = "") {
    if (val1.toLowerCase() === val2.toLowerCase()) {
        return true
    }
    return false;
}
// На сайте Reddit ссылки на разделы (сабреддиты) формируются следующим образом: https://reddit.com/r/название_раздела/. Напишите функцию, которая принимает ссылку на раздел и возвращает название раздела.
function reddit(url = "") {
    n = url.split("/")
    return n[n.length - 2];
}
// Напишите функцию reverseAndNegate(arr), которая принимает массив чисел и возвращает перевернутый отрицательный массив.
let newArr = [];
function reverseAndNegate(array) {
    for (let i = array.length - 1; i >= 0; i--) {
        newArr.push(array[i] * -1);
    }
    console.log(newArr);
};
// Напишите функцию, которая принимает массив чисел и сначала к каждому чётному числу прибавляет 4, у каждого нечётного отнимает 2, потом убирает из массива числа, делящиеся на 13 без остатка и возвращает сумму оставшихся чисел.
function arrWork(array = []) {
    for (let i = 0; i < array.length; i++) {
        if (array[i] % 2 == 0) {
            val = array[i] + 4;
            newArr.push(val)
        } else {
            val = array[i] - 2;
            newArr.push(val)
        }
    };
    console.log(newArr);
    let sum = 0;
    for (let i = 0; i <= newArr.length - 1; i++) {
        if (newArr[i] % 13 === 0) {
        } else {
            sum += newArr[i];
        }
    }
    console.log(sum);
}
// Напишите функцию transformUpvotes(arr), которая принимает массив сокращенных записей количества лайков ['10k', '2.3k', '5k', '32', '28.4k'] и возвращает массив чисел [10000, 2300, 5000, 32, 28400].
function transformUpvotes(array = []) {
    for (let i = 0; i < array.length  ; i++) {
        const element = array[i];
        if (element.includes("k")) {
            nel =  element.replaceAll("k","");
            newArr.push(+nel * 1000)
            continue
        }
        newArr.push(+element)
    };
    console.log(newArr);
}