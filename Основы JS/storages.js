
// пример вывода кук и настройки на клиенте (не рекомендуется)
function justCookie() {
  console.log("document.cookie", document.cookie);

  const btn = document.querySelector(".j-btn-set-cookie");
  btn.addEventListener("click", setCookie);
  function setCookie() {

    document.cookie = "test_cookie_client=testCookie; max-age=3600;";
    console.log("document.cookie", document.cookie);
  }
}


// пример взаимодействия с localStorage
function justLocalStorage() {
  let myKey = localStorage.getItem('myKey');
  console.log('1. myKey', myKey);
  localStorage.setItem('myKey', 'myValue');

  // JSON, который мы будем записывать
  const jsonString = `
{
  "book": "Harry Potter"
}
`;
  // Запишем данные в localStorage в виде JSON
  // Получаем данные по ключу myKey в localStorage
  localStorage.setItem('myJSON', jsonString);
  myKey = localStorage.getItem('myKey');
  console.log('2. myKey', myKey);

  // Получаем данные по ключу myJSON в localStorage
  // Выводим сразу как объект
  // Удаляем данные по ключу myJSON в localStorage
  const myJSON = localStorage.getItem('myJSON');
  console.log('3. myJSON', JSON.parse(myJSON));
  localStorage.removeItem('myJSON');

  // Проверяем, какие данные остались
  // Очищаем весь localStorage
  console.log('4. myKey', localStorage.getItem('myKey'));
  console.log('5. myJSON', localStorage.getItem('myJSON'));
  localStorage.clear();

  // Проверяем, что все удалено
  console.log('6. myKey', localStorage.getItem('myKey'));
  console.log('7. myJSON', localStorage.getItem('myJSON'));
}

// пример взаимодействия с localStorage
function justSessionStorage() {
  // Получаем данные по ключу myKey в sessionStorage
  // Если sessionStorage очищался или же закрывались все вкладки https://codepen.io/, то ключа не будет.
  // Если не зачищался - получим значение по ключу
  let myKey = sessionStorage.getItem('myKey');
  console.log('1. myKey', myKey);

  // Запишем данные в sessionStorage в виде простой строки
  // Получаем данные по ключу myKey в localStorage
  sessionStorage.setItem('myKey', 'myValue');
  myKey = sessionStorage.getItem('myKey');
  console.log('2. myKey', myKey);

  // При перезагрузки страницы в обоих случаях выведется значение
  // "myValue", так как сессия еще не закончилась.
  // Если вы закроете все вкладки https://codepen.io/ и откроете
  // пример заново, то в первом случае данных не будет,
  // а во втором, после записи, уже будут
}




// Пример выполнения XHR запроса и сохранения результата в LocalStorage
function XHRLocalStorageRequest() {

  // DOM 
  const btnRequestNode = document.querySelector('.j-btn-request');
  const btnClearNode = document.querySelector('.j-btn-clear');

  // Вешаем обработчик на кнопку для запроса
  btnRequestNode.addEventListener('click', () => {
    // Получаем данные по ключу myJSON в localStorage
    const myJSON = localStorage.getItem('myJSON');
    if (myJSON) {
      // Если данные в localStorage есть - просто выводим их
      console.log('localStorage JSON', JSON.parse(myJSON));
    } else {
      // Если данных в localStorage нет - делаем запрос
      useRequest('https://picsum.photos/v2/list/?limit=1', (json) => {
        // Выводим данные, полученные в результате запроса
        console.log('request JSON', json);
        // Записываем результат запроса в localStorage
        localStorage.setItem('myJSON', JSON.stringify(json));
      });
    }
  });

  // Вешаем обработчик на кнопку для очистки localStorage
  btnClearNode.addEventListener('click', () => {
    localStorage.clear();
    console.log('Данные из localStorage удалены');
  });


  function useRequest(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);

    xhr.onload = function () {
      if (xhr.status != 200) {
        console.log('Статус ответа: ', xhr.status);
      } else {
        const result = JSON.parse(xhr.response);
        if (callback) {
          callback(result);
        }
      }
    };
    xhr.onerror = function () {
      console.log('Ошибка! Статус ответа: ', xhr.status);
    };
    xhr.send();
  };
}



// запуск
document.addEventListener("DOMContentLoaded", somefunc);

