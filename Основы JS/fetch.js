
// Пример работы fetch
function justFetch() {
    const btn = document.querySelector('.j-btn');
    btn.addEventListener('click', () => {
        // Делаем запрос за данными
        fetch('https://picsum.photos/v2/list/?limit=5')
            .then((response) => {
                // Объект ответа на запрос
                console.log('response', response);
                // Превращаем объект в JSON. Мы не можем его сразу прочитать,
                // надо отдать в следующий then
                const result = response.json();
                console.log('result', result);
                return result;
            })
            .then((data) => {
                // Объект результата в формате JSON
                console.log(data);
            })
            .catch(() => { console.log('error') });
    });

    // сокращенная запись
    // const ok = fetch('https://jsonplaceholder.typicode.com/todos/1')
    //     .then(response => response.json())
    //     .then(json => json)
    //     .catch(error => console.error(error));


}

// Пример работы fetch c опциями GET 
function justFetchOtpsGET() {
    // У метода fetch есть объект настроек, который позволяет настраивать HTTP-запрос:
    const options = {
        method: 'GET', // выбор метода запроса
        mode: 'cors', // режим работы запроса
        headers: { // дополнительные заголовки для запроса
            'Content-Type': 'application/json'
        },
        body: 'body', // тело запроса
        // и тд
    }
    fetch('https://picsum.photos/v2/list/?limit=5', options)
}

// 
// Пример работы fetch c опциями POST 
function x() {
    const btn = document.querySelector('.j-btn');
    btn.addEventListener('click', () => {
        // Настраиваем наш запрос
        const options = {
            // Будем использовать метод POST
            method: 'POST',
            // Добавим тело запроса
            body: JSON.stringify({
                title: 'foo',
                body: 'bar',
                userId: 1
            }),
            // Дополнительный заголовое с описанием типа данных,
            // которые мы отправляем серверу. В данном случае
            // сервер jsonplaceholder будет знать, как ему
            // обрабатывать запрос
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }
        // Делаем запрос за данными
        fetch('https://jsonplaceholder.typicode.com/posts', options)
            .then(response => response.json())
            .then(json => console.log(json))
    });
}


// Пример работы fetch как acync
// Можно выполнять несколько fetch в рамках одной задачи
function acyncFetch() {
    const btn = document.querySelector('.j-btn');

    // Функция, которая возвращает fetch
    const useRequest = () => {
        return fetch('https://picsum.photos/v2/list/?limit=5')
            .then((response) => {
                console.log('response', response);
                return response.json();
            })
            .then((json) => { return json; })
            .catch(() => { console.log('error') });
    }
    btn.addEventListener('click', async () => {
        console.log('start');
        const requestResult = await useRequest(); // ждем пока запрос выполнится
        console.log('requestResult', requestResult);
        console.log('end');
    });
}


// Пример запроса к внешнему api через fetch результат вставляется в ODM
// Пример API запроса: https://api.openweathermap.org/data/2.5/weather?q=Москва&appid=59aaed6f10d8ae0565183dd571a3b596&units=metric
function pageLoaded() {
    // <main>
    //     <div id="frame">
    //         <h1>Узнать прогноз погоды</h1>
    //         <input id="input" type="text" placeholder="Введите город" />
    //         <button id="button">Узнать</button>
    //         <div id="output"></div>
    //     </div>
    // </main>

    // const apiURL = "https://api.openweathermap.org/data/2.5/weather";
    const apiKey = "59aaed6f10d8ae0565183dd571a3b596";

    const input = document.querySelector("#input");
    const btn = document.querySelector("#button");
    const output = document.querySelector("#output");

    function sendRequest() {
        if (input.value !== '') {
            fetch(`https://api.openweathermap.org/data/2.5/weather?q=${input.value}&appid=${apiKey}&units=metric`)
                .then(response => {
                    return response.json();
                })
                .then(data => {``
                    writeOutput(formatOutput(data));
                })
        }
    }

    function formatOutput(data) {
        let output = `
      <p>${data.main.temp} &deg;C</p>
      <p>${data.weather[0].main}</p>
      <img src="http://openweathermap.org/img/wn/${data.weather[0].icon}.png" />
    `
        return output;
    }

    function writeOutput(message) {
        output.innerHTML = message;
    }
    btn.addEventListener("click", sendRequest);
}

// запуск
document.addEventListener("DOMContentLoaded", somefunc);