// Пример своего промиса
function customPromise() {
  // Флаг вызова resolve или reject. Можно переключать
  // для изменения результата
  let flag = false;

  // Создаем promise
  const myPromise = new Promise((resolve, reject) => {
    if (flag) {
      resolve("Успешное выполнение promise");
    } else {
      reject("Неуспешное выполнение promise");
    }
  });

  // Выполняем promise
  myPromise
    .then((result) => {
      console.log('Обрабатываем resolve', result);
    })
    .catch((error) => {
      console.log('Обрабатываем reject', error);
    })
    .finally(() => {
      console.log('Выполняется всегда');
    });
}

// Пример своего промиса
function customPromiseAdvance() {

  console.log('Запускаем функцию c promise');
  usePromise();
  console.log('Функция выполнилась');


  // Функция выполнения promise
  function usePromise() {
    // Создаем promise
    const myPromise = new Promise((resolve, reject) => {
      console.log('1');
      setTimeout(() => {
        console.log('2');
        resolve("Успешное выполнение promise");
      }, 1000);
      console.log('3');
    });

    console.log('4');
    // Выполняем promise
    myPromise
      .then((result) => {
        console.log('Обрабатываем resolve', result);
      })
      .catch((error) => {
        console.log('Обрабатываем reject', error);
      })
      .finally(() => {
        console.log('Выполняется всегда');
      });

    console.log('5');
  };

  // Запускаем функцию с promise
  //  1
  //  3
  //  4
  //  5
  //  Функция выполнилась
  // undefined
  //  2
  //  Обрабатываем resolve Успешное выполнение promise
  //  Выполняется всегда
}

// запуск
document.addEventListener("DOMContentLoaded", somefunc);