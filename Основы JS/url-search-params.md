# URLSearchParams

## Вызов конструктора без аргументов

```
const params = new URLSearchParams()
params.get('field')  // undefined
```

## Вызов конструктора со строкой GET-параметров в качестве аргумента
```
const params = new URLSearchParams(window.location.search)  // '?field=value'
params.get('field')  // 'value'
```



- `.append(name, value)`	Добавляет новое значение по ключу name, все существующие значения с ключом name сохраняет.
- `.set(name, value)`	Добавляет новое значение по ключу name, все существующие значения с ключом name удаляет.
- `.delete(name)`	Удаляет все значения по ключу name.
- `.get(name)	`Возвращает значение по ключу name.
- `.getAll(name)`	Возвращает все значения по ключу name.
- `.has(name)`	Проверяет, есть ли значение с ключом name.
- `.entries()`	Возвращает итератор по парам ключ-значение.
- `.values()`	Возвращает итератор по значениям.
- `.keys()	`Возвращает итератор по ключам.
- `.sort()`	Сортирует параметры в алфавитном порядке ключей.
- `.forEach((key, value, params) => { … })	`Позволяет перебрать параметры парами по аналогии с методом Array.forEach.

- Внимание! Объект URLSearchParams не является наследником Array, хоть и имеет аналогичные методы, поэтому не обладает другими методами, такими как .map, .filter, .reduce.


## Вариант формирования параметров с добавлением в текущие GET-параметры страницы нового значения и удалением старого, а также перебор параметров с помощью for ... of:


```
const params = new URLSearchParams(window.location.search)  // ?q=search&variant=slow
params.set('q', 'newSearch')  // set new parameter
params.delete('variant')  // delete parameter
params.toString()  // '?q=newSearch'
for (let [name, value] of x) {
    console.log(name, value)  // print each parameter
}
```

## Вариант формирования параметров для запроса с помощью Fetch API из готового объекта, а также сортировка и перебор параметров с помощью forEach:

```
const params = new URLSearchParams({
  b: 1,
  a: 2,
})

params.sort()
params.forEach((name, value) => {
  console.log(name, value)  // print each parameter
})

fetch(`/path${params}`)  // /path?a=2&b=1
```
