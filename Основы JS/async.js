
// пример запускаа и остановки повторяющегося действия по кнопкам
// действие асинхронно
function basic() {

  // <button class="btn j-start">Кликните для исполнения кода</button>
  // <button class="btn j-end">Кликните для остановки таймера</button>

  const btnStart = document.querySelector('.j-start');
  const btnEnd = document.querySelector('.j-end');

  // Переменная для записи id запущенного setInterval
  let ID = null;

  function f1() {
    console.log('1');
  };

  function f2() {
    console.log('2');
  };

  btnStart.addEventListener('click', () => {
    f2(); // Выполняется сразу
    // Если   
    if (!ID) {
      ID = setInterval(f1, 1000); // Выполняется через каждую 1 секунду 
    }
    f2(); // Выполняется сразу
  });

  btnEnd.addEventListener('click', () => {
    // останавливаем функцию setInterval
    clearInterval(ID);
    ID = null;
  });
}


// пример показывает разницу синхронного XHR запроса и асинхронного запроса
function seeDefinitions() {


  // <button class="btn j-btn-async-request">Асинхронный запрос данных</button>
  // <button class="btn j-btn-sync-request">Синхронный запрос данных</button>

  // Урл для запроса. Возвращает 4k картинку, которая много весит.
  const reqUrl = "https://i.picsum.photos/id/237/4096/2160.jpg";

  // JS анимация. Необходимо для наглядного примера
  const animNode = document.querySelector('.j-anim--block');
  const steps = ['(0, 0)', '(20px, 0)', '(20px, 20px)', '(0, 20px)'];
  let stepValue = 0;
  setInterval(() => {
    if (!steps[stepValue]) { stepValue = 0; }
    stepValue += 1;
    animNode.style = `transform: translate${steps[stepValue]};`
  }, 300);


  //  Функция-обертка над XMLHttpRequest, осуществляющая запрос
  //  async - флаг вкл/выкл асинхронности
  function useRequest(async) {
    var xhr = new XMLHttpRequest();
    console.log('1');
    xhr.open('GET', reqUrl, async);
    console.log('2');
    xhr.send();
    console.log('3');
    console.log('4 Статус ответа: ', xhr.status);
    xhr.onload = function () {
      console.log('5 Статус ответа: ', xhr.status);
    };
    console.log('6');
  };

  const btnAsync = document.querySelector('.j-btn-async-request');
  const btnSync = document.querySelector('.j-btn-sync-request');

  // Не кнопку вешаем обработчик асинхронного запроса
  btnAsync.addEventListener('click', () => {
    useRequest(true);
  });

  // На кнопку вешаем обработчик синхронного запроса
  btnSync.addEventListener('click', () => {
    useRequest(false);
  });
}

// запуск
document.addEventListener("DOMContentLoaded", somefunc);