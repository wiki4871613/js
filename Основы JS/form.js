
function justFormData() {
  // Объект FormData может быть создан как без аргумента, так и с объектом тега <form> в качестве арг. 
  // В случае вызова конструктора без параметров, будет создан пустой объект
  const dataNew = new FormData()
  dataNew.get('field')  // undefined

  // <form id="myForm">
  //   <input type="text" name="field" value="value">
  //   <!-- ... -->
  // </form>

  // В случае вызова с тегом <form> будет создан объект   и в него будут прочитаны данные из формы.
  // Получить все значения из формы
  const myForm = document.getElementById('myForm')
  const data = new FormData(myForm)
  data.get('field')  // 'value'
  data.getAll('field')

  // итерация по полям
  for (let [name, value] of data) {
    console.log(name, value)
  }
  // .append(name, value)	Добавляет новое значение по ключу name, все существующие значения с ключом name сохраняет.
  // .append(name, blob, fileName) Добавляет файл по ключу name с именем fileName, все существующие файлы с name сохраняет.
  // .set(name, value)	Добавляет новое значение по ключу name, все существующие значения с ключом name удаляет.
  // .set(name, blob, fileName)	Добавляет файл по ключу name с именем fileName, все существующие файлы с name удаляет.
  // .delete(name)	Удаляет все значения по ключу name.
  // .get(name)	Возвращает значение по ключу name.
  // .getAll(name)	Возвращает все значения по ключу name.
  // .has(name)	Проверяет, есть ли значение с ключом name.
  // .entries()	Возвращает итератор по парам ключ-значение.
  // .values()	Возвращает итератор по значениям.
  // .keys()	Возвращает итератор по ключам.
}

// Вариант отправки простой формы через Fetch API с текстовым,
// числовым, множественными датами и булевым полями
function customFormData() {
  //альтернатива <input type="checkbox" name="checkbox">
  const data = new FormData()
  data.append('text', 'value')
  data.append('number', 1)
  data.append('checkbox', 'on')
  data.append('dates', new Date().toISOString())
  data.append('dates', '2022-07-12T17:10:39.595Z')
  fetch('/', { body: data })
}

//Вариант отправки формы с дополнительным текстовым полем и дополнительным изображением,
// нарисованным на канвасе, с помощью XHR or fetch
function customFormData() {
  const form = document.getElementById('myForm')
  const canvas = document.createElement('canvas')

  // ... draw image
  canvas.toBlob((blob) => {  // get the image blob
    const data = new FormData(form)  // read data from form
    data.append('field', 'value')  // add new text field=value
    data.append('file', blob, 'file.jpg')  // add file field with name file.jpg

    // send form data with XHR
    const xhr = new XMLHttpRequest()
    xhr.open('POST', '/handleFormSubmission')
    xhr.send(data)
    // or
    fetch('/handleFormSubmission', {
      method: 'POST',
      body: data,
    })
  })
}




// отправка данных формы #myForm с помощью Fetch API по адресу GET /handle,
// добавив файл из переменной fileBlob с названием qr.jpg в поле формы files 
// (не удаляя уже имеющиеся там файлы), если в форме стоит галочка
// `<input type="checkbox" name="saveQR">`.
function customFormData2() {
  const fileBlob = '...';
  const form = document.getElementById('myForm');
  const data = new FormData(form);
  if (data.has('saveQR')) {
    data.append("files", fileBlob, "qr.jpg");
  }
  fetch('/handle', { body: data })
}