// Напишите функцию getInfo(persons), которая принимает массив объектов, описывающих человека, имеющих поля {name, age}, и возвращает объект со средним возрастом и именем самого старшего человека.
function getInfo(persons = []) {
    let obj = {
        ageMiddle: 0,
        oldest: ""
    };
    let age = 0;
    persons.forEach(person => {
        if (person.age > age) {
            age = person.age;
            obj.oldest = person.name;
        }
        obj.ageMiddle += person.age;
    });
    obj.ageMiddle = obj.ageMiddle / persons.length;
    return obj;
};
persons = [{ age: 10, name: "poul" }, { age: 20, name: "joel" }, { age: 10, name: "viktoria" }];

// Напишите функцию, которая принимает массив массивов вида [['ключ1', 'значение1'], ['ключ2', 'значение2']] и возвращает объект вида: {ключ1: 'значение1', ключ2: 'значение2'}.
function convert(array = []) {
    let obj = {};
    array.forEach(element => {
        obj[element[0]] = element[1];
    });
    console.log(obj);
}

// Напишите функцию countChars(str), которая принимает строку и возвращает количество букв в этой строке.
function countChars(str = "") {
    let obj = {};
    for (const key in str) {
        if (typeof (obj[str[key]]) == 'undefined') {
            obj[str[key]] = 1;
        } else {
            obj[str[key]] = obj[str[key]] + 1;
        }
    }
    console.log(obj);
}

// Напишите функцию countEverySecond(n), которая каждую секунду печатает в консоль цифру, начиная с 1 и заканчивая {n}.
function countEverySecond(n) {
    console.log("OK");
}
setTimeout(countEverySecond, 1000);
