

// пример отправки GET запроса (получаем json)
// ответ вставляем в DOM
function XHRsend() {

  // <body>
  //     <h1>XMLHttpRequest_2</h1>
  //     <button class="btn j-btn-request">Запрашиваем данные</button>
  //     <div class="result j-result">Здесь будет результат запроса</div>
  // </body>


  const resultNode = document.querySelector('.j-result');
  const btnNode = document.querySelector('.j-btn-request');


  // Вешаем обработчик на кнопку для запроса
  btnNode.addEventListener('click', () => {
    makeUseRequest('https://picsum.photos/v2/list/?limit=5', displayResult);
  })

  // * Функция-обертка над XMLHttpRequest, осуществляющая запрос
  // * url - урл, по которому будет осуществляться запрос
  // * callback - функция, которая вызовется при успешном выполнении
  // * и первым параметром получит объект-результат запроса
  function makeUseRequest(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true); //Функция инициализации запроса.

    xhr.onload = function () { // Свойство, которому можно присвоить callback-функцию, которая сработает, если запрос успешно отработал.
      if (xhr.status != 200) {
        console.log('Статус ответа: ', xhr.status);
      } else {
        const result = JSON.parse(xhr.response);
        if (callback) {
          callback(result);
        }
      }
    };
    xhr.onerror = function () { // Свойство, которому можно присвоить callback-функцию, которая сработает, если запрос завершился ошибкой.
      console.log('Ошибка! Статус ответа: ', xhr.status);
    };
    xhr.send(); // Метод отправки запроса.
  };

  // * Функция обработки полученного результата
  // * apiData - объект с результатом запроса
  function displayResult(apiData) {
    let cards = '';
    apiData.forEach(item => {
      const cardBlock = `
        <div class="card">
          <img
            src="${item.download_url}"
            class="card-image"
          />
          <p>${item.author}</p>
        </div>
      `;
      cards = cards + cardBlock;
    });
    resultNode.innerHTML = cards;
  }
}



// пример отправки GET запроса (получаем json)
// ответ вставляем в DOM
function pageLoaded() {

  //<main>
  //   <div id="frame">
  //     <h1>Перевести число в двоичную систему</h1>
  //     <input id="input" type="text" placeholder="Введите число" />
  //     <button id="button">Перевести</button>
  //     <div id="output"></div>
  //   </div>
  // </main>



  const input = document.querySelector("#input");
  const btn = document.querySelector("#button");
  const output = document.querySelector("#output");

  btn.addEventListener("click", sendRequest);

  function sendRequest() {
    if (validateInput()) {
      let xhr = new XMLHttpRequest();
      xhr.open("GET", `https://api.math.tools/numbers/base?number=${input.value}&from=10&to=2`);
      xhr.send();

      xhr.onerror = function () {
        writeOutput('При отправке запроса произошла ошибка');
      }

      xhr.onload = function () {
        if (xhr.status == 200) {
          let data = JSON.parse(xhr.response);
          writeOutput(`Ответ: ${data.contents.answer}`);
        }
      }
    }
  }

  function writeOutput(message) {
    output.innerText = message;
  }

  function validateInput() {
    let validated = true;
    if (input.value === '' || isNaN(+input.value)) {
      validated = false;
    }
    return validated;
  }
}


// запуск
document.addEventListener("DOMContentLoaded", somefunc);