const countdown = (value, fn) => {
  fn(value);
  return value > 0 ? countdown(value - 1, fn) : value;
};
countdown(10, value => console.log(value));

// 10
// 9
// 8
// ...
// 0

// Рекурсия — это паттерн, который особенно хорошо работает с асинхронными
// процессами. Функции могут восстанавливать себя по готовности, например,
// когда становятся доступными данные или когда таймер заканчивает работу.


// Функцию countdown можно изменить, чтобы отсчет происходил с задержкой.
const countdown2 = (value, fn, delay = 1000) => {
  fn(value);
  return value > 0
    ? setTimeout(() => countdown(value - 1, fn, delay), delay)
    : value;
};
const log = value => console.log(value);
countdown(10, log);