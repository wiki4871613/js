// Оператор распространения (...) выполняет несколько задач. Во-первых, он по-
// зволяет комбинировать содержимое массивов. Например, с его помощью можно
// создать массив, в котором объединены два других массива:

const peaks = ["Tallac", "Ralston", "Rose"];
const canyons = ["Ward", "Blackwood"];
const tahoe = [...peaks, ...canyons];

// получить последний элемент из массива
const [last] = [...peaks].reverse();
console.log(last); // Rose

// Поскольку для копирования массива мы использовали оператор распростра-
// нения, массив peaks остался нетронутым и может быть использован позже
// в исходной форме.

// Оператор распространения также можно использовать для получения остав-
// шихся элементов в массиве:

const lakes = ["Donner", "Marlette", "Fallen Leaf", "Cascade"];
const [first, ...others] = lakes;
console.log(others.join(", ")); // Marlette, Fallen Leaf, Cascade


// оператор распространения можно использовать для объектов
const morning = {
  breakfast: "oatmeal",
  lunch: "peanut butter and jelly"
};

const dinner = "mac";
const backpackingMeals = {
  ...morning,
  dinner
};

