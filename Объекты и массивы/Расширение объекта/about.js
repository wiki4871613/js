// Расширение объектного литерала противоположно деструктуризации.

const n = "Tallac";
const elevation = 9738;
const print = function() {
  console.log(`Mt. ${this.name} is ${this.elevation} feet tall`);
  };

const funHike = { n, elevation, print };
funHike.print(); // Mt. Tallac is 9738 feet tall