// Чистая функция — это функция, которая возвращает значение, вычисленное
// на основе ее аргументов. Чистые функции принимают по крайней мере один
// аргумент и всегда возвращают значение или другую функцию. Они не вы-
// зывают побочных эффектов, поскольку не изменяют глобальные переменные
// и состояние приложения. Их аргументы рассматриваются как неизменяемые
// данные.

// ЧИСТЫЕ ФУНКЦИИ ПРИГОДНЫ ДЛЯ ТЕСТИРОВАНИЯ

const frederick = {
  name: "Siesta Cloud",
  canRead: false,
  canWrite: false
};

const selfEducate = person => ({
  ...person,
  canRead: true,
  canWrite: true
});

console.log(selfEducate(frederick));
console.log(frederick);

// Такая версия selfEducate является чистой функцией. Она вычисляет значение
// на основе переданного ей аргумента person, возвращает новый объект person
// без изменения аргумента и не имеет побочных эффектов.


// Чистые функции — еще одна основная концепция функционального програм-
// мирования. Они сделают написание кода намного проще, так как во время ра-
// боты не повлияют на состояние вашего приложения. При написании функций
// старайтесь следовать трем правилам:

// 1. Функция должна принимать хотя бы один аргумент.
// 2. Функция должна возвращать значение или другую функцию.
// 3. Функция не должна изменять переданный ей аргумент.