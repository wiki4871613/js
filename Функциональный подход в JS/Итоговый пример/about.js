

const oneSecond = () => 1000; // возв одну секунду;
const getCurrnetTime = () => new Date() // возв текущее время;
const clear = () => console.clear(); // очищает консоль
const log = message => console.log(message); // выводит сообщение в консоль

// Принимает объект времени и возвращает объект, который содержит часы, минуты и секунды.
const serializeTime = date => ({
  hours: date.getHours(),
  minutes: date.getMinutes(),
  seconds: date.getSeconds(),
});

// Принимает объект показания часов и возвращает объект, в котором показание
// преобразуется в формат гражданского времени.
const civilianHours = clockTime => ({
  ...clockTime,
  hours: clockTime.hours > 12 ? clockTime.hours - 12 : clockTime.hours,
})

// Принимает объект показания часов и добавляет к нему время суток (AM или PM).
const appendAMPM = clockTime => ({
  ...clockTime,
  ampm: clockTime.hours >= 12 ? "PM" : "AM"
})

// Принимает целевую функцию и возвращает функцию, которая передает
// время в адрес цели. В этом примере целью будет метод console.log.
const display = target => time => target(time);

// Принимает шаблонную строку — hh:mm:ss tt — и использует ее для возврата
// показания часов, отформатированного по критериям, заданным строкой.
// То есть эта функция заменяет заполнители показаниями часов, минут, секунд
// и времени суток.
const formatClock = format => time =>
  format
    .replace("hh", time.hours)
    .replace("mm", time.minutes)
    .replace("ss", time.seconds)
    .replace("tt", time.ampm)

// Принимает ключ объекта в качестве аргумента и добавляет ноль перед
// значением, хранящемся под ключом этого объекта. Функция получает
// ключ к указанному полю и добавляет перед значениями ноль, если они
// меньше 10.
const prependZero = key => clockTime => ({
  ...clockTime,
  key: clockTime[key] < 10 ? "0" + clockTime[key] : clockTime[key]
});

// Принимает показания часов в качестве аргумента и преобразует их в формат
// гражданского времени, используя обе его формы.
const convertToCivilianTime = clockTime => compose(
  appendAMPM,
  civilianHours
)(clockTime)

// Принимает показания часов в формате гражданского времени и обеспечивает
// рендеринг двузначных цифр в часах, минутах и секундах, добавляя нули.
const doubleDigits = civilianTime => compose(
  prependZero("hours"),
  prependZero("minutes"),
  prependZero("seconds"),
)(civilianTime)

// Запускает часы, вызывая функции обратного вызова с интервалом в одну секунду
const startTicking = () =>
  setInterval(compose(
    clear, //очищаю консоль
    getCurrnetTime, // получаю текущее время
    serializeTime, // создаю свой обьект и заполняю поля
    convertToCivilianTime, // добавляю AM PM и преобразую поля своего обьекта в гражданский формат 
    doubleDigits, // (02,03... или 09..)
    formatClock("hh:mm:ss tt"),
    display(log),
  ),
    oneSecond());

const compose = (...fns) => arg =>
  fns.reduce((composed, f) => f(composed), arg);


// Однако у этого подхода есть несколько преимуществ. Эти функции легко
// тестировать и использовать повторно. Их можно использовать в будущих часах
// или других цифровых дисплеях. Программа легко масштабируется. Побочных
// эффектов нет. Вне самих функций глобальных переменных нет. Ошибки могут
// возникнуть, но их будет легко найти.