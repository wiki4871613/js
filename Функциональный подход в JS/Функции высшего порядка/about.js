// JavaScript поддерживает функциональное программирование, потому что
// функции в этом языке являются объектами первого класса. Это означает, что
// они могут делать то же, что и переменные. В последнем синтаксисе JavaScript
// есть языковые улучшения, которые могут усилить методы функционального
// программирования. К ним относятся стрелочные функции, промисы и оператор
// распространения.


var log = function (message) {
  console.log(message);
};

log("In JavaScript, functions are variables");
// Функции в JavaScript являются переменными


const log = message => {
  console.log(message);
};

// Они также могут быть возвращены из других функций, как и переменные:
const createScream = function (logger) {
  return function (message) {
    logger(message.toUpperCase() + "!!!");
  };
};
const scream = createScream(message => console.log(message));
scream("fn can be returned");


// Последний пример относится к функциям высшего порядка, которые либо
// принимают, либо возвращают другие функции. Можно описать ту же функцию
// высшего порядка createScream с помощью стрелок:

const createScream2 = logger => message => {
  logger(message.toUpperCase() + "!!!");
};

// Можно сказать, что JavaScript — функциональный язык, поскольку его функции
// относятся к объектам первого класса. То есть функции являются данными. Их
// можно сохранять, извлекать или передавать через приложения, как переменные.



const invokeIf = (condition, fnTrue, fnFalse) =>
  condition ? fnTrue() : fnFalse();

const showWelcome = () => console.log("Welcome!!!");
const showUnauthorized = () => console.log("Unauthorized!!!");

invokeIf(true, showWelcome, showUnauthorized); // "Welcome!!!"

invokeIf(false, showWelcome, showUnauthorized); // "Unauthorized!!!"




const userLogs = userName => message =>
  console.log(`${userName} -> ${message}`);

const log = userLogs("grandpa23");
log("attempted to load 20 fake members");

getFakeMembers(20).then(
  members => log(`successfully loaded ${members.length} members`),
  error => log("encountered an error loading members")
);
// grandpa23 -> попытка загрузки 20 фиктивных сотрудников
// grandpa23 -> успешно загружены 20 сотрудников
// grandpa23 -> попытка загрузки 20 фиктивных сотрудников
// grandpa23 -> при загрузке сотрудников возникла ошибка