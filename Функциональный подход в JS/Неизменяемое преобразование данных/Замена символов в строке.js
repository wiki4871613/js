// Использование метода string.replace — это способ описания того, что должно произойти: замена пробелов в строке.
const string = "Restaurants in Hanalei";
const urlFriendly = string.replace(/ /g, "-");
console.log(urlFriendly);
