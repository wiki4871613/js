
let list = [{ title: "Rad Red" }, { title: "Lawn" }, { title: "Party Pink" }];
const addColor = (title, array) => array.concat({ title });
// or

// Эта функция копирует исходный список в новый массив, а затем добавляет в эту копию новый объект, содержащий название цвета. Результат неизменяемый.
const addColor2 = (title, list) => [...list, { title }];


// Метод Array.concat объединяет массивы: берет новый объект с новым заголовком и добавляет его к копии исходного массива.
console.log(addColor("Glam Green", list).length); // 4
console.log(list.length); // 3

