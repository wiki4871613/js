const schools = ["Yorktown", "Washington & Liberty", "Wakefield"];

const wSchools = schools.filter(school => school[0] === "W");
console.log(wSchools);
// ["Washington & Liberty", "Wakefield"]


// Array.filter — это встроенная функция JavaScript, которая создает новый мас-
// сив из исходного массива. Она принимает на вход предикат — функцию, которая
// всегда возвращает логическое значение: истина или ложь. Array.filter вызывает
// этот предикат для каждого элемента в массиве. Затем элементы передаются
// в предикат в качестве аргументов, и возвращаемые значения используются,
// чтобы решить, будет ли конкретный элемент добавлен в новый массив. В дан-
// ном случае Array.filter проверяет, начинается ли название каждой гимназии
// с буквы «W».

const cutSchool = (cut, list) => list.filter(school => school !== cut);

console.log(cutSchool("Washington & Liberty", schools).join(", "));
// "Yorktown, Wakefield"

// cutSchool — это чистая функция. Она берет массив
// гимназий и название гимназии, которое следует удалить, и возвращает новый
// массив без этого названия.