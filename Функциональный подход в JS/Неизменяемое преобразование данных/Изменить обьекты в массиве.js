const schools = ["Yorktown", "Washington & Liberty", "Wakefield"];

const highSchools = schools.map(school => `${school} High School`);
console.log(highSchools.join("\n"));

// ф-н будет вызываться один раз для каждого элемента в мас-
// сиве, и все, что она вернет, будет добавлено в новый массив


// Вот пример функции map, возвращающей объект для каждой гимназии:

const highSchools2 = schools.map(school => ({ name: school }));

// [
// { name: "Yorktown" },
// { name: "Washington & Liberty" },
// { name: "Wakefield" }
// ]

const editName = (oldName, name, arr) =>
  arr.map(item => {
    if (item.name === oldName) {
      return {
        ...item,
        name
      };
    } else {
      return item;
    }
  });
// or 
const editName2 = (oldName, name, arr) =>
  arr.map(item => (item.name === oldName ? { ...item, name } : item));


const ages = [21, 18, 42, 40, 64, 63, 34];
const maxAge = ages.reduce((max, age) => {
  console.log(`${age} > ${max} = ${age > max}`);
  if (age > max) {
    return age;
  } else {
    return max;
  }
}, 0);

// 21 > 0 = true
// 18 > 21 = false
// 42 > 21 = true
// 40 > 42 = false
// 64 > 42 = true
// 63 > 64 = false
// 34 > 64 = false
// maxAge 64

const max = ages.reduce((max, value) => (value > max ? value : max), 0);
