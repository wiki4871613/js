
let color_lawn = {
  title: "lawn",
  color: "#00FF00",
  rating: 0
};

const rateColor = function (color, rating) {
  return Object.assign({}, color, { rating: rating });
};
// or
const rateColor2 = (color, rating) => ({
  ...color,
  rating
});

// Теперь у нас есть объект с новым цветом, причем старый код не изменился.
console.log(rateColor(color_lawn, 5).rating); // 5
console.log(color_lawn.rating); // 0

