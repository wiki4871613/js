// Модуль JavaScript — это фрагмент кода, который можно многократно исполь-
// зовать и включать в другие файлы JavaScript, не вызывая конфликтов пере-
// менных. Модули JavaScript хранятся в отдельных файлах, по одному файлу на
// модуль. Существует два варианта экспорта модуля: экспорт нескольких объектов
// JavaScript из одного модуля или одного объекта JavaScript для каждого модуля.


export const print = (message) => log(message, new Date())

export const log = (message, timestamp) =>
  console.log(`${timestamp.toString()}: ${message}`)

export default new Expedition("Mt. Freel", 2, ["water", "snack"]);

// ------------------------

import { print, log } from "./about";
// or
import { print as p, log as l } from "./about";
// or
import * as fns from './text-helpers'

print("printing a message");
log("logging a message");
p("printing a message");
l("logging a message");

