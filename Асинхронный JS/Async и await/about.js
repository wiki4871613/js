// Еще один популярный подход к работе с промисами — это создание функции async
const getFakePerson = async () => {
  try {
    let res = await fetch("https://api.randomuser.me/?nat=US&results=1");
    let { results } = res.json();
    console.log(results);
  } catch (error) {
    console.error(error);
  }
};
getFakePerson();


// Функция getFakePerson объявляется с использованием
// ключевого слова async. Это делает функцию асинхронной, и она может до-
// ждаться разрешения промисов, прежде чем выполнять код дальше. Ключевое
// слово await используется перед вызовами промисов. Оно приказывает функции
// дождаться разрешения промиса.