const some_list = [
  { name: "A", elem: 10 },
  { name: "B", elem: 20 },
  { name: "V", elem: 30 }
];


// before
export default function App() {
  return (
    <ul>
      {some_list.map((item, i) => (
        <li key={i}>
          {item.name} — {item.elem.toLocaleString()}
        </li>
      ))}
    </ul>
  );
}


// after
export default function App() {
  return (
    <List
      data={some_list}
      renderEmpty={<p>This list is empty</p>}
      renderItem={item => (
        <>
          {item.name} — {item.elem.toLocaleString()}
        </>
      )}
    />
  );
}

function List({ data = [], renderItem, renderEmpty }) {
  return !data.length ? renderEmpty
    : (
      <ul>
        {data.map((item, i) => (
          <li key={i}>{renderItem(item)}</li>
        ))}
      </ul>
    );
}