import React, { useState } from "react";

export const MyForm = () => {
  
  // const [inputOne, setInputOne] = useState("");
  // const [inputTwo, setInputTwo] = useState("");

  const [inputOneProps, resetInputOne] = useInput("");
  const [inputTwoProps, resetInputTwo] = useInput("");

  const submit = e => {
    e.preventDefault();
    // setTitle("");
    // setColor("");
    resetInputOne();
    resetInputTwo();
  };
  
  return (
    <form onSubmit={submit}>
      <input
        // value={title}
        // onChange={event => setTitle(event.target.value)}
        {...inputOneProps}
        type="text"
        placeholder="color title..."
      />
      <input
        // value={color}
        // onChange={event => setColor(event.target.value)}
        {...inputTwoProps}
        type="text"
      />
      <button>Отправить</button>
    </form>
  );
}

const useInput = initialValue => {
  const [value, setValue] = useState(initialValue)
  return [
    { value, onChange: e => setValue(e.target.value) },
    () => setValue(initialValue)
  ]
}