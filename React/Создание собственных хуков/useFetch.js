import { useState, useEffect } from 'react';

function useFetch(url) {
  const [data, setData] = useState(null);

  useEffect(() => {
    fetch(url)
      .then(res => res.json())
      .then(data => setData(data))
      .catch(error => console.log(error));
  }, [url]);

  return data;
}

function ExampleComponent() {
  const data = useFetch('https://jsonplaceholder.typicode.com/todos/1');
  return (
    <div>
      {data ? <div>{data.title}</div> : <div>Загрузка...</div>}
    </div>
  );
}