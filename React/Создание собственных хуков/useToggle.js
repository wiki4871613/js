import { useState, useCallback } from 'react';

function useToggle(initialValue) {
  const [value, setValue] = useState(initialValue);

  const toggle = useCallback(() => {
    setValue(v => !v);
  }, []);

  return [value, toggle];
}

function App() {
  const [isDarkMode, changeDarkMode] = useToggle(true);

  return (
    <button onClick={changeDarkMode}>
      Переключить цветовую схему
    </button>
  );
}