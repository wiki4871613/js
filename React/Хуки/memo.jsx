import { useMemo } from 'react';

const MovieList = ({ movies, tab }) => {

  const visibleMovie = useMemo(
    () => filterMovies(movies, tab),
    [todos, tab]
  );

  // ...
}

// В примере выше мы создаём мемоизированную переменную visibleMovie. Эта переменная является результатом выполнения функции filterMovies с аргументами movies и tab.
// Теперь ключевой момент — переменная будет пересчитана заново только тогда, когда todos или tab изменят своё значение. В противном случае, что бы не происходило, эта переменная лишний раз пересчитываться не будет. Точно также как и useEffect — он не будет перезапущен, если хотя бы одна из зависимостей не изменится.



import { useState } from 'react';

const App = () => {
  const [clicks, updateClicks] = useState(0);

  // const handleClickBigButton = () => {
  //   updateClicks((curValue) => curValue + 4321);
  // }

  const handleClickBigButton = useCallback(() => {
    updateClicks((curValue) => curValue + 4321);
  }, [updateClicks]);

  return (
    <>
      Кликов: {clicks}
      <button
        onClick={() => {
          updateClicks(clicks + 1)
        }}
      >
        Жми!
      </button>
      <BigButton handleClick={handleClickBigButton} />
    </>
  );
}


import React from "react";

const BigButton = (props) => {
  const { handleClick } = props;

  console.log("Рендер BigButton");

  return <button onClick={handleClick}>БОЛЬШАЯ КНОПКА!</button>;
};

export default React.memo(BigButton);
