


export const withAnalysis = (WrappedComponent) => {
  
  const UpdatedComponent = (props) => { 

    const onClick = (...params) => {
      analysis.click(WrappedComponent.name, new Date().getTime());

      if (props.onClick) {
        props.onClick.apply(null, params);
      }
    }

    const newProps = { ...props };


    if (props.onClick) newProps.onClick = onClick;


    return <WrappedComponent {...newProps} />
  }


  return UpdatedComponent;
}